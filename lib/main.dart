import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const request = "https://api.hgbrasil.com/finance?format=json&key=0745218c";

void main() async {
  runApp(MaterialApp(
      home: Home(),
      theme: ThemeData(hintColor: Colors.amber, primaryColor: Colors.white)));
}

Future<Map> getData() async {
  http.Response response = await http.get(request);
  return json.decode(response.body);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double dolar, euro, real,bitCoin, libra;
  
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();
  final bitCoinController = TextEditingController();
  final libraController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text("Conversor \$"),
          backgroundColor: Colors.amber,
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: resetFields,
            )
          ],
        ),
        body: getScrollView()
    );
  }
  
  void realChanged(String text) {
    double real = double.parse(text);
    dolarController.text = (real / dolar).toStringAsFixed(2);
    euroController.text = (real / euro).toStringAsFixed(2);
    bitCoinController.text = (real / bitCoin).toStringAsFixed(2);
    libraController.text = (real / libra).toStringAsFixed(2);
  }
  
  void dolarChanged(String text) {
    double dolar = double.parse(text);
    realController.text = (dolar * this.dolar).toStringAsFixed(2);
    euroController.text = (dolar * this.dolar / euro).toStringAsFixed(2);
    bitCoinController.text = (dolar * this.dolar / bitCoin).toStringAsFixed(2);
    libraController.text = (dolar * this.dolar / libra).toStringAsFixed(2);
  }
  
  void euroChanged(String text) {
    double euro = double.parse(text);
    realController.text = (euro * this.euro).toStringAsFixed(2);
    dolarController.text = (euro * this.euro / dolar).toStringAsFixed(2);
    bitCoinController.text = (euro * this.euro / bitCoin).toStringAsFixed(2);
    libraController.text = (euro * this.euro / libra).toStringAsFixed(2);
  }
  
  void bitCoinChanged(String text) {
    double bitCoin = double.parse(text);
    realController.text = (bitCoin * this.bitCoin).toStringAsFixed(2);
    dolarController.text = (bitCoin * this.bitCoin / dolar).toStringAsFixed(2);
    euroController.text = (bitCoin * this.bitCoin / euro).toStringAsFixed(2);
    libraController.text = (bitCoin * this.bitCoin / libra).toStringAsFixed(2);
  }
  
  void libraChanged(String text) {
    double libra = double.parse(text);
    realController.text = (libra * this.libra).toStringAsFixed(2);
    dolarController.text = (libra * this.libra / dolar).toStringAsFixed(2);
    euroController.text = (libra * this.libra / euro).toStringAsFixed(2);
    bitCoinController.text = (libra * this.libra / bitCoin).toStringAsFixed(2);
  }
  
  void resetFields() {
    realController.text = "";
    dolarController.text = "";
    euroController.text = "";
    bitCoinController.text = "";
    libraController.text = "";
  }
  
  Widget getScrollView() {
    return SingleChildScrollView(
      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
      child: FutureBuilder<Map>(
          future: getData(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Center(
                    child: Text("Carregando Dados...",
                        style: TextStyle(color: Colors.amber, fontSize: 25),
                        textAlign: TextAlign.center));
              default:
                if (snapshot.hasError) {
                  return Center(
                      child: Text("Erro ao Carregar Dados :(",
                          style: TextStyle(
                              color: Colors.amber, fontSize: 25),
                          textAlign: TextAlign.center)
                  );
                } else {
                  dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                  euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];
                  bitCoin =
                  snapshot.data["results"]["currencies"]["BTC"]["buy"];
                  libra = snapshot.data["results"]["currencies"]["GBP"]["buy"];
                  return SingleChildScrollView(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Icon(Icons.monetization_on, size: 150, color: Colors.amber),
                        buildTextField("Real", "R\$", realController, realChanged),
                        Divider(),
                        buildTextField("Dolar", "\$", dolarController, dolarChanged),
                        Divider(),
                        buildTextField("Euro", "€", euroController, euroChanged),
                        Divider(),
                        buildTextField("Libra", "£", libraController, libraChanged),
                        Divider(),
                        buildTextField("Bitcoin", "₿", bitCoinController, bitCoinChanged),
                      ],
                    ),
                  );
                }
            }
          }),
    );
  }
  
  Widget buildTextField(String label, String prefix, TextEditingController c,
      Function f) {
    return TextField(
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.amber),
        border: OutlineInputBorder(),
        prefixText: prefix,
      ),
      style: TextStyle(color: Colors.amber, fontSize: 25),
      controller: c,
      onChanged: f,
      keyboardType: TextInputType.number,
    );
  }
}
